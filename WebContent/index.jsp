<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="CSS/style.css">	
	<!-- Title -->
	<title> Moodle </title>
</head>
<body>

	<!-- //Contenedor -->
	<div class="container-fluid">
	
		<jsp:include page="HTML/header.html"></jsp:include>
		
		<jsp:include page="JSP/login.jsp"></jsp:include>
		
		<jsp:include page="HTML/footer.html"></jsp:include>
	
	</div>

	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script type="JS/bootstrap.min.js"></script>
	<script type="JS/jquery-3.3.1.slim.min.js"></script>
	<script type="JS/popper.min.js"></script>
	
</body>
</html>