<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="CSS/style.css">	
	<!-- Title -->
	<title> Moodle </title>
</head>
<body>
		
	<!-- //Fila(row) del login -->
	<div class="row justify-content-center p-5">
		<!-- //Contenedor formuario login -->
		<div class="col-4">
			<form class="form-horizontal">
				<div class="form-group">
					<label for="user"> Usuario </label>
					<input type="text" name="txtUser" class="form-control" placeholder="Usuario" id="user">
				</div>
				<div class="form-group">
					<label for="password"> Contraseņa </label>
					<input type="password" name="txtPassword" class="form-control" placeholder="Contraseņa">
				</div>
				<button type="submit" class="btn btn-dark btn-lg btn-block" id="submit"> Enviar </button>
			</form>
		</div>
	</div>
	
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script type="JS/bootstrap.min.js"></script>
	<script type="JS/jquery-3.3.1.slim.min.js"></script>
	<script type="JS/popper.min.js"></script>	
		
</body>
</html>