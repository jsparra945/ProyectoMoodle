<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="CSS/style.css">	
	<!-- Title -->
	<title> Moodle </title>
</head>
<body>

	<!-- //Contenedor -->
	<div class="container-fluid">
	
		<jsp:include page="../HTML/header.html"></jsp:include>
		
		<!-- //Fila(row) del carrusel -->
		<div class="row justify-content-center p-5">
			<!-- //Contenedor Carrusel -->
			<div class="carousel slide" id="carouselImages" data-ride="carousel">
				<!-- //Indicadores Inferior de las imagenes -->
				<ul class="carousel-indicators">
					<li data-target="#carouselImages" data-slide-to="0" class="active"></li>
					<li data-target="#carouselImages" data-slide-to="1"></li>
					<li data-target="#carouselImages" data-slide-to="2"></li>
				</ul>
				<!-- //Diapositivas con su respectiva imagen -->
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img alt="Imagen1" src="../IMAGES/imagen.jpg" width="1100" height="500">
					</div>
					<div class="carousel-item">
						<img alt="Imagen2" src="../IMAGES/img.jpg" width="1100" height="500">
					</div>
					<div class="carousel-item">
						<img alt="Imagen3" src="../IMAGES/film.jpg" width="1100" height="500">
					</div>
				</div>
				<!-- //Controles para cambiar de imagenes -->
				<a class="carousel-control-prev" href="#carouselImages" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				</a>
				<a class="carousel-control-next" href="#carouselImages" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				</a>
			</div>
		</div>
		
		<jsp:include page="../HTML/footer.html"></jsp:include>
		
	</div>

	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script type="JS/bootstrap.min.js"></script>
	<script type="JS/jquery-3.3.1.slim.min.js"></script>
	<script type="JS/popper.min.js"></script>
	
</body>
</html>